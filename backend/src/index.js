const express = require('express');

const mongoose = require('mongoose');
const cors = require('cors');

const routes = require('./routes');

const app = express();

mongoose.connect('mongodb+srv://fabiosales:fabio12345678@cluster0-ncgvy.mongodb.net/week10?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});

app.use(cors());
app.use(express.json()); // configurando o express para entender o formato json
app.use(routes);


// portar da aplicação
app.listen(3333);