const { Router } = require('express');
const DevController = require('./controller/DevController')
const SearchController = require('./controller/SearchController')


const routes = Router();


// metodos HTTP: GET, POST, PUT DELETE

// Query params (GET): req.query (filtros, paginação, ordenação)

// Router params (PUT, DELETE) : reequest.params (identificar recurso na alteração ou remoção)

// Body (PUT, POST): reques.body (dados para criação ou alteração de um registro)


// MongoDB (banco não relacional)


routes

  // DEVS Endpoint
  .get('/devs', DevController.index)
  .post('/devs', DevController.create)
  .get('/devs/:github', DevController.read)
  .put('/devs/:github', DevController.update)
  .delete('/devs/:github', DevController.delete)

  // SEARCH Endpoint
  .get('/search', SearchController.index);

module.exports = routes;
